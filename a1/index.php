<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This an online portfolio showing what I learned in LIS4381-Mobile Web Application Development.">
		<meta name="author" content="Andrea Colon-De Feria">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Installations and Git Commands</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> <br> This assignment was separated into three parts. Distributed Version Control with Bitbucket, Development Installations, and Chapter Questions (Chs 1, 2). There are screenshots of AMPPS running, Android Studio, and Java installations.  
				</p>
				

				<h4>Java Installation</h4>
				<img src="img/helloMeEmulatorScreenshot.PNG" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/testProjScreenshot.PNG" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/amppsScreenshot.PNG" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
