> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
# LIS4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### Assignment 1 Requirements:

*Three parts:*

1. Distributed Version Control with Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi)
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Git commands w/short descriptions:

1. git init - Initiates git, and lets user access and edit a local repository. 
2. git status - Provides the status of all of the files created or changed, for instance, those that need to be commited or added.
3. git add - Add files or directories onto local repository.
4. git commit - Lets users commit changes made locally, prior to sending to remote repository.
5. git push - Allows users to push local entities into their remote repositories.
6. git pull - Allows users to pull, or accquire, the changes from a remote server to a local repository. 
7. git config - This command allows users to set repository options.

#### Assignment Screenshots:

 *Screenshot of AMPPS running [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi)*| *Screenshot of running java Hello*:                        | *Screenshot of Android Studio - My First App*:                             
 :---------------------------------------------------------------------------------------:|:----------------------------------------------------------:|:--------------------------------------------------------------------------:
 [![AMPPS Installation Screenshot](img/amppsScreenshot.PNG)](img/amppsScreenshot.PNG)                                | [![JDK Installation Screenshot](img/testProjScreenshot.PNG)](img/testProjScreenshot.PNG) |[![Android Studio Installation Screenshot](img/helloMeEmulatorScreenshot.PNG)](img/helloMeEmulatorScreenshot.PNG)

* ^ Click each image to view it larger ^
#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc19k/bitbucketstationlocations/ "Bitbucket Station Locations")



