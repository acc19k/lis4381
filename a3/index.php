<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This an online portfolio showing what I learned in LIS4381-Mobile Web Application Development.">
		<meta name="author" content="Andrea Colon-De Feria">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - ERDs and My Events Application</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> <br> For this assignment we created an ERD based on business ruled, created an application on Android Studio which would calculate ticket prices, and included borders on images within the app as well as custom buttons. Also, there are screenshots of Java skillsets 4-6. 
				</p>
				<p>Here are links to the ERD Resources: </p>
				<ol>
					<li> <a href="a3/docs/a3_lis4381_model.mwb">ERD Model</a></li>
					<li> <a href="a3/docs/a3.sql">SQL file</a></li>

				</ol>
				
				<h3>MySQL ERD Screenshots</h3>

				<h4>Screenshot of pet store ERD Model </h4>
				<img src="img/a3ERDScreenshot.png" class="img-responsive center-block" alt="ERD Screenshot">

				<h4>Screenshot of table 1 with 10 values </h4>
				<img src="img/a3TableScreenshot1.png" class="img-responsive center-block" alt="table screenshot 1">

				<h4>Screenshot of table 2 with 10 values </h4>
				<img src="img/a3TableScreenshot2.png" class="img-responsive center-block" alt="table screenshot 2">

				<h4>Screenshot of table 3 with 10 values </h4>
				<img src="img/a3TableScreenshot3.png" class="img-responsive center-block" alt="table screenhot 3">

				<h3>My Events Application Screenshots</h3>

				<h4>Screenshot of main app </h4>
				<img src="img/a3Screenshot2.png" class="img-responsive center-block" alt="My Events app screenshot 1">

				<h4>Screenshot of ticket number selection in app </h4>
				<img src="img/a3Screenshot2.png" class="img-responsive center-block" alt="My Events app screenshot 2">

				<h4>Screenshot of group selection in app </h4>
				<img src="img/a3Screenshot3.png" class="img-responsive center-block" alt="My Events app screenshot 3">

				<h4>Screenshot of ticket price calculation </h4>
				<img src="img/a3Screenshot4.png" class="img-responsive center-block" alt="My Events app screenshot 4">

				<h3>Skillset screenshots</h3>

				<h4>Java Skillset 4: Decision Structures </h4>
				<img src="skillSets/ss4_Decision_Structures/ss4_img/ss4Screenshot1.png" class="img-responsive center-block" alt="ss4">

				<h4>Java Skillset 5: Random Array</h4>
				<img src="skillSets/ss5__Random_Array/ss5_img/ss5Screenshot.png" class="img-responsive center-block" alt="ss5">

				<h4>Java Skillset 6: Methods </h4>
				<img src="skillSets/ss6_Methods/ss6_img/ss6Screenshot.png" class="img-responsive center-block" alt="ss6">
				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
