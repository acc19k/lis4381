

# LIS 4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### Assignment 3 Requirements:

*The following will provide:*

1. Create an ERD based upon business rules
2. Provide screenshots of completed ERD and values
3. Provide DB resource links
4. Create application to showcase price of concerts
5. Add border to an image on Android Studio
6. Customize button on application
7. Provide screenshots of functioning app
8. Skillsets 4-6 with screenshots


#### MySQL ERD Screenshots:

*Screenshot of ERD model*
![Screenshot or ERD](img/a3ERDScreenshot.png)

*Screenshot of table 1 with ten values*
![Table 1 screenshot](img/a3TableScreenshot1.png)

*Screenshot of table 2 with ten values*
![Table 2 Screenshot](img/a3TableScreenshot2.png)

*Screenshot of table 3 with ten values*
![Table 3 Screenshot](img/a3TableScreenshot3.png)

*Link to ERD:* [ERD file link](/a3/docs/a3_lis4381_model.mwb)
*Link to SQL file:* [sql file link](/a3/docs/a3.sql)

#### My Events Application Screenshots:

|  *Screenshot of My Events app: main*  |  *Screenshot of My Events app: selecting ticket number*  |  *Screenshot of My Events app: selecting group*  |  *Screenshot of My Events app: calculation*  |
|---------------------------------------|----------------------------------------------------------|--------------------------------------------------|----------------------------------------------|
| ![app screenshot 1](img/a3Screenshot1.png)  |  ![app screenshot 2](img/a3Screenshot2.png)  |  ![app screenshot 3](img/a3Screenshot3.png)  |  ![app screenshot 4](img/a3Screenshot4.png) |


#### Skillset Screenshots:

| *Skillset 4 screenshot* | *Skillset 5 screenshot*  | *Skillset 6 screenshot*  |
|-------------------------|--------------------------|--------------------------|
| ![ss4](../skillSets/ss4_Decision_Structures/ss4_img/ss4Screenshot1.png)  | ![skillset 5](../skillSets/ss5__Random_Array/ss5_img/ss5Screenshot.png)   | ![skillset 6](../skillSets/ss6_Methods/ss6_img/ss6Screenshot.png)  |








