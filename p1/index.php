<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This an online portfolio showing what I learned in LIS4381-Mobile Web Application Development.">
		<meta name="author" content="Andrea Colon-De Feria ">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - My Business Card app</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> <br>In this project, we had to create an application with two interfaces showing our business cards. We were instructed to find ways to make the button in the application customized by adding shading and a border, as well as inclulde a border around the photo of ourselves, and create a favicon for our apps. There are also screenshots of the Java skillsets required for this project as well as an extra credit one.  
				</p>

				<h3>Project Screenshots</h3>
				<h4>Screenshot of main page in My Business Card App</h4>
				<img src="img/p1_Screenshot1.png" class="img-responsive center-block" alt="Main business card page">

				<h4>Screenshot of Business Card info on the app</h4>
				<img src="img/p1_Screenshot2.png" class="img-responsive center-block" alt="Business card info page">

				<h3>Skillset Screenshots</h3>
				<h4>Java Skillset 7: Random Array Data Validation </h4> 
				<img src="skillSets/ss7_Random_Array_Data_Validation/ss7_Screenshot.png" class="img-responsive center-block" alt="ss7">

				<h4>Java Skillset 8: Largest of Three Numbers</h4>
				<img src="skillSets/ss8_Largest_Three_Numbers/ss8_Screenshot.png" class="img-responsive center-block" alt="ss8">

				<h4>Java Skillset 9: Array Runtime Data Validation </h4>
				<img src="skillSets/ss9_Array_Runtime_Data_Validation/ss9_Screenshot.png" class="img-responsive center-block" alt="ss9">

				<h4>Java Skillset: Rain Detector </h4>
				<img src="skillSets/Rain_detector/rainDetector_Screenshot.png" class="img-responsive center-block" alt="ssEC">
				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
