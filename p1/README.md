# LIS 4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### Project 1 Requirements:

*The following will provide:*

1. Create an app displaying virtual business card
2. Use launcher icon throughout entire application
3. Screenshots of running My Business Card app
4. Java skillsets 7-9

### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1
* Screenshot of running application's first user interface (Business card main)
* Screenshot of running application's second user interface (Business card details)
* Screenshot of skillsets

Click on a screenshot to view it larger, if needed!
&nbsp;
&nbsp;

### Project Screenshots:
|*Screenshot of main page in My Business Card app*|*Screenshot of business card page in My Business Card app*|
|:-----------------------------------------------:|:--------------------------------------------------------:|
| [![Project1 Screenshot](img/p1_Screenshot1.png)](img/p1_Screenshot1.png) | [![Project1 Screenshot2](img/p1_Screenshot2.png)](img/p1_Screenshot2.png)        |

&nbsp;
&nbsp;

### Skillset Screenshots:
|*Java skillset 7:Random array data validation*|*Java skillset 8:Largest of three numbers*|
|----------------------------------------------|------------------------------------------|
| [![ss7 screenshot](../skillSets/ss7_Random_Array_Data_Validation/ss7_Screenshot.png)](../skillSets/ss7_Random_Array_Data_Validation/ss7_Screenshot.png) | [![ss8 screenshot](../skillSets/ss8_Largest_Three_Numbers/ss8_Screenshot.png)](../skillSets/ss8_Largest_Three_Numbers/ss8_Screenshot.png) |

&nbsp;

|*Java skillset 9:Array runtime data validation* | *Java Skillset extra credit:Rain detection* |
|------------------------------------------------|---------------------------------------------|
| [![ss9 screenshot](../skillSets/ss8_Largest_Three_Numbers/ss8_Screenshot.png)](../skillSets/ss8_Largest_Three_Numbers/ss8_Screenshot.png) | [![rain detection screenshot](../skillSets/Rain_detector/rainDetector_Screenshot.png)](../skillSets/Rain_detector/rainDetector_Screenshot.png) |
