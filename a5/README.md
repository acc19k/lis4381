

# LIS 4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### Assignment 5 Requirements:

*This assignment requires:*

1. A4 cloned files
2. Review subdirectories and files
3. Use index.php from A4 to successfully create the files needed for server-side validation
4. Ensure petstore database (A3) is linked and can add data.

#### README.md file should include the following items:

* Screenshots of server-side validation working and of data being added to the database
* Link to local LIS4381 webapp --> [Click here to view webapp!](http://localhost/repos/lis4381/ "Local LIS4381 Web app")
* Screenshots of skillsets 13-15


#### Assignment Screenshots:

|  *Screenshot 1 of error in server-side validaton*  |  *Screenshot 2 of error in server-side validation*  |    
|---------------------------------------|----------------------------------------------------------|
| ![error1](img/a5_Screenshot4.png)  |  ![errorpage2](img/a5_Screenshot5.png)  |    


| *Screenshot of correct data input* |*Screenshot values being added to petsore table as 11th value* | 
|-----------------------------------|-----------------------------------|
|  ![correct data input](img/a5_Screenshot2.png)  |![added values](img/a5_Screenshot3.png) |

#### Skillset Screenshots:

| *Skillset 13: Sphere Volume Converter* | *Skillset 14: PHP simple calculator screenshot 1* |
|----------------------------------------|--------------------------------------------------|
|![ss13](../skillSets/ss13_Sphere_Volume_Calculator/ss13_screenshot.png)  |  ![ss14/1](../simple_calculator/img/ss14_screenshot1.png)  |

|*Skillset 14: PHP simple calculator screenshot 2* | *Skillset 14: PHP simple calculator screenshot 3*  |  *Skillset 14: PHP simple calculator screenshot 4* |
|--------------------------------------------------|----------------------------------------------------|----------------------------------------------------|
| ![ss14/2](../simple_calculator/img/ss14_screenshot2.png) | ![ss14/3](../simple_calculator/img/ss14_screenshot3.png) | ![ss14/4](../simple_calculator/img/ss14_screenshot4.png)  |

|*Skillset 15: Write/read file screenshot 1*  |  *Skillset 15: Write/read file screenshot 2*  |
|---------------------------------------------|-----------------------------------------------|
|![ss15/1](../write_read_file/img/ss15_screenshot1.png)  | image will be here once available  |