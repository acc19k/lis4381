

# LIS 4381 -- Mobile Web Application Development

## Andrea Colon-De Feria

### Assignment 4 Requirements:

*Requirements:*

1. Create My Online Portfolio web app
2. Clone bit bucket repository for resources
3. Use Bootstrap 
4. Add image links or buttons onto Bootstrap carousel slides
5. Use jQuery for document validation
6. Java Skillsets 10-12

#### README.md file should include the following items:

* Screenshots of jQuery validation and My Online Portfolio home
* Link to local LIS4381 web app: [Click here to view webapp!](http://localhost/repos/lis4381/ "Local LIS4381 Web app")
  

#### Assignment Screenshots:

|  *Screenshot of carousel slide 1: Link to Bit Bucket*  |  *Screenshot of carousel slide 2: Link to Linked In*  |  *Screenshot of carousel slide 3: Link to Handshake*  |
|---------------------------------------|----------------------------------------------------------|--------------------------------------------------|
| ![Bit Bucket Link](img/homeScreenshot1.png)  |  ![Linked In](img/homeScreenshot2.png)  |  ![HandShake](img/homeScreenshot3.png)  |

| *Screenshot of failed validation* | *Screenshot of passed validation* |
|-----------------------------------|-----------------------------------|
| ![failed](img/failedValidation.png) |![passed](img/passedValidation.png) |


#### Skillset Screenshots:

| *Skillset 10: Array List * | *Skillset 11: Alphanumeric Special*  | *Skillset 12: Temperature Connversion*  |
|-------------------------|--------------------------|--------------------------|
| ![ss10](../skillSets/ss10_Array_List/ss10Screenshot.png)  | ![skillset 11](../skillSets/ss11_Alphanumeric_Special/ss11Screenshot.png)  | ![skillset 12](../skillSets/ss12_Temperature_Conversion/ss12Screenshot.png)  |




