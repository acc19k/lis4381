

# LIS 4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### Project 2 Requirements:

*This project requires:*

1. Modify A5 php files to include "edit" and "delete" functionality to database.
2. Ensure server-side validation for "edit" still works.
3. Add RSS feed to test directory.

#### README.md file should include the following items:

* Screenshot of "edit" button failed server-side validation and passed update 
* Screenshot of "delete" button working and deleting data
* Screenshot of RSS feed
* Link to local LIS4381 webapp --> [Click here to view webapp!](http://localhost/repos/lis4381/ "Local LIS4381 Web app")

#### Assignment Screenshots (Click a screenshot to view it larger):


| *Entity chosen to edit: Mascota Mall petstore* | *Validation attempt to update Mascota Mall name* | 
|------------------------------------------------|--------------------------------------------------|
| [![Edit list](img/p2_screenshot1.png)](img/p2_screenshot1.png)          | [![Validation attempt, wrong name](img/p2_screenshot2.png)](img/p2_screenshot2.png)  |


| *Failed Update validation* | *Updated Mascota Mall to Mascota Mall new name* |
|----------------------------|-------------------------------------------------|
| [![Failed update validation](img/p2_screenshot3.png)](img/p2_screenshot3.png) | [![New name, passed validation](img/p2_screenshot8.png)](img/p2_screenshot8.png) |



| *New 12th entity created to delete*  |  *Delete validation screen*  | *Successful deletion. Now 11 entities in table* |
|--------------------------------------|------------------------------|-------------------------------------------------|
|[![new entity to delete](img/p2_screenshot4.png)](img/p2_screenshot4.png) |  [![confirm deletion popup](img/p2_screenshot5.png)](img/p2_screenshot5.png) | [![deletion success](img/p2_screenshot6.png)](img/p2_screenshot6.png) |


*RSS Feed with Nintendo Voice Chat podcast episodes!*
[![NVC podcast RSS feed](img/p2_screenshot7.png)](img/p2_screenshot7.png)