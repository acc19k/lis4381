<?php
ini_set('log_errors',1);
ini_set('error_log', dirname(__FILE__). 'error_log.txt');
error_reporting(E_ALL);

//get the item ID
$pst_id_v = $_POST['pst_id'];
//exit($pst_id_v); if you want to test and know what it is

require_once('../global/connection.php');

//15:21 of helper video 2
//delete query MAKE SURE TO HAVE WHERE CLAUSE
$query = 
"DELETE FROM petstore
WHERE pst_id = :pst_id_p";

try{
    $statement = $db->prepare($query);
    $statement->bindParam(':pst_id_p', $pst_id_v);
    $row_count = $statement->execute();
    $statement->closeCursor();


    header('Location: index.php');
}

catch (PSOException $e)
{
    $error = $e->getMessage();
    include('../global/error.php');
}

?>

