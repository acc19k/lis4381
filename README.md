
# LIS4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations)
    * Provide git command desciptions
2. [A2 README.md](a2/README.md)
    * Create Healthy Recipes Android app
    * Provide screenshots of completed app
    * Java skillsets 1-3
3. [A3 README.md](a3/README.md)
    * Create ERD based upon business rules
    * Provide screenshots of completed ERD
    * Provide DB resource links
    * Java skillsets 4-6
4. [A4 README.md](a4/README.md)
    * Create My Online Portfolio web app
    * Clone bit bucket repository for resources
    * Use Bootstrap 
    * Add image links or buttons onto Bootstrap carousel slides
    * Use jQuery for document validation
    * Java Skillsets 10-12
5. [A5 README.md](a5/README.md)
    * A4 cloned files
    * Review subdirectories and files
    * Modify given index.php file and duplicate to effectively add petsored for server-side validation
    * Skillsets 13-15 
6. [P1 README.md](p1/README.md)
    * Create an app displaying virtual business card
    * Use launcher icon throughout entire application
    * Add borders to app images
    * Add text shadows to button text
    * Add background colors to app activities
    * Java skillsets 7-9
7. [P2 README.md](p2/README.md)
    * Modify A5 php files to include "edit" and "delete" functionality to database.
    * Ensure server-side validation for "edit" still works.
    * Provide screeshots of testing.
    * Add RSS feed to test directory. 
