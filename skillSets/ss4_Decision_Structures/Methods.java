import java.util.Scanner;

public class Methods { 

//create method without returning any value or object aka void
public static void getRequirements()
{ 
    //display operational messeges
    System.out.println("Developer: Andrea Colon-De Feria");
    System.out.println("Program evaluates user-entered characters.");
    System.out.println("Use the following characters: W or w, C or c, H or h, N or n.");
    System.out.println("Use following decision structures: If...else, and switch.");
    System.out.println();

}//end of getrequirements method


public static void getUserPhoneType()
{
    Scanner sc = new Scanner(System.in);
    String myStr = "";
    char myChar= '\0';
    //note that this API method is not set to get a character from the Scanner, so we gotta get a string usign scanner.next() to involde charAt(0) to get a character

    System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
    System.out.print("Please enter phone type: ");
    myStr = sc.next().toLowerCase();
    myChar = myStr.charAt(0);

    System.out.println("\nif...else");

    if (myChar == 'w') System.out.println("Phone type: work.");
    else if (myChar =='c') System.out.println("Phone type: cell.");
    else if (myChar == 'h') System.out.println("Phone type: home.");
    else if (myChar == 'n') System.out.println("Phone type: none.");
    else System.out.println("Incorrect character entry.");


    System.out.println();
    System.out.println("Switch: ");
    switch(myChar)
    {
            case 'w':
                System.out.println("Phone type: work.");
                break;
            case 'c':
                System.out.println("Phone type: cell.");
                break;
            case 'h':
                System.out.println("Phone type: home.");
                break;
            case 'n':
                System.out.println("Phone type: none.");
                break;
            default:
                System.out.println("Incorrect character entry");

        }

}//end of getUserPhoneType



}//end of Methods