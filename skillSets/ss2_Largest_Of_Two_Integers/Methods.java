//Problem: Create a program that asks user for teo integers and returns which of the two is is larger

import java.util.Scanner; //this file we have to import from library

public class Methods
{

    //here is the first method that that inroduces people to the program. So, no object because it doesn't retun anything, just prints:
    public static void getRequirements()
    {
        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("This program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");

        System.out.println();
    } //end of getRequirements Method

    public static void evaluateNumber(){

        int x;
        int y;
        //create a new scanner object
        Scanner sc = new Scanner(System.in);

        //ask for integers and assign to variables
        System.out.println("Enter first integer: ");
        x = sc.nextInt();
        System.out.println("Enter second integer: ");
        y = sc.nextInt();

        System.out.println(); //blank line for readibility
        //now we gotta calculate which is larger with an else if statement 

        if( x > y ){
            System.out.println(x + " is larger than " + y);
        }
        else if(y > x){
            System.out.println(y + " is larger than " + x);
        }
        else{
            System.out.println("Integers are the same!");
        }


    } //end of evaluateNumber method
}//end of class