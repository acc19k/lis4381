import java.util.Scanner;
import java.util.Random;

public class Methods {
    
    public static void getRequirements(){
        //operatuonal messeges
        System.out.println("Print minumum and maximum integer values.");
        System.out.println("Program prompts user to enterdesired number of pseudorandom-generated integers (min1).");
        System.out.println("Program validates user input for integers greater than 0.");
        System.out.println("Use following loop structuress: for, enhanced for, while, do...while.\n");

        //print min and max values
        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);

        System.out.println(); //print blank line

    }//end of geteRequirements

    //now we make a value-returning method that is static since it requires no object
    public static int[] createArray(){

        Scanner sc = new Scanner(System.in);
        int arraySize = 0;

        //prompt users for randomly generated numbers
        System.out.print("Enter desired number of pseudo-randomintegers (min 1): ");
        while (!sc.hasNextInt())
        {
            System.out.println("Not Valid integer!\n");
            sc.next(); // if you do not write this, ther ewill be an infinite loop!
            System.out.print("Please try again. Enter valid integer (min 1): ");
        }

        arraySize = sc.nextInt(); //valid int

        while(arraySize < 1){
            //include data validation
            System.out.print("\nNumber must be greater than 0. Please enter integer greater than 0: ");
            while(!sc.hasNextInt()){
                System.out.print("\nNumber must be integer: ");
                sc.next();
                System.out.print("Please try again. Enter value greater than 0: ");
            } 
            arraySize = sc.nextInt(); //valid int greater than 0
        }//end of create array

    int yourArray[] = new int[arraySize];
    return yourArray;



}//end of methods

public static void generatePseudoRandomNumbers(int [] myArray){

    Random r = new Random(); //instantiate random object variable
    int i = 0;
    System.out.println("for loop: ");
    for(i=0;i<myArray.length;i++){
        System.out.println(r.nextInt());
    }

    System.out.println("\nEnhanced for loop: ");
    for(int n:myArray){
        System.out.println(r.nextInt());
        i++;
    }

    i = 0; //reassing to zero
    System.out.println("\ndo...while loop: ");
    do{
        System.out.println(r.nextInt());
        i++;
    } while(i<myArray.length);

}//end of random number

}
