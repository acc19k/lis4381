class Main{
    public static void main(String args[]){
        //call static void methods
        Methods.getRequirements();
        //initialize array, the size will be determined by the user, as seen in methods.java
        int arraySize=0;
        arraySize = Methods.validateArraySize();

        //call method by passing the info you got from the method above
        //basically, the method will call another method inside
        Methods.calculateNumbers(arraySize);
    }
}