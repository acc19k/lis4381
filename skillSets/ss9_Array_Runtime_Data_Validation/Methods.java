import java.util.Scanner; 
class Methods{
    //create global scanner object so it is used in more than one method
    //using final prevents object variable from being changed
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements(){

        System.out.println("1) Program created array size at run-time.");
        System.out.println("2) Program displays array size.");
        System.out.println ("3) Program rounds sum and average of numbers to two decimal places.");
        System.out.println("4) Numbers *must* be float data type, not double.");

        System.out.println();

        
    }//end of getrequirements

    public static int validateArraySize(){
        int arraySize = 0;

        System.out.print("Please enter array size: ");
        while (!sc.hasNextInt()){
            System.out.println("Not valid integer!\n");
            sc.next(); //IMPORTANT
            System.out.print("Please try again. Enter array size: ");

        }//end of while

        arraySize = sc.nextInt();
        System.out.println();

        //return this int 
        return arraySize;
    }//end of validateArraySize

    public static void calculateNumbers(int arraySize){
        float sum = 0.0f;
        float average = 0.0F;

        //indicate number of values required based on user input
        System.out.print("Please enter " + arraySize + " numbers.\n");

        //create an array to store what the user input based on the size that the user chose
        float numsArray[] = new float[arraySize];

        //validate data entry
        for(int i = 0; i < arraySize ; i++){
            System.out.println("Enter number " + (i + 1) + " : ");
            while (!sc.hasNextFloat()){
                System.out.println("Not valid number!\n");
                sc.next();
                System.out.print("Please try again. Enter number " + (i+1) + ": ");
            }
            numsArray[i] = sc.nextFloat();
            sum = sum + numsArray[i];
        }
        average = sum / arraySize;

        //print the numbers entered 
        System.out.print("\nNumbers entered:");
        for(int i = 0; i < numsArray.length; i++) System.out.print(numsArray[i] + " ");

        printNumbers(sum, average);


    }// end of calculateNumbers

    public static void printNumbers(float sum, float average){
        System.out.println("\nSum: " + String.format("%.2f", sum));
        System.out.println("Average: " + String.format("%.2f" , average));
    }//end of printnumbers
}