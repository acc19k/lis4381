//javadoc: documentation generator for generating API documentation in HTML format from Java source code

class Main{
public static void main(String[] args) {
    //here we call static void methods, so no objects are going to be returned
    Methods.getRequirements();
    Methods.valildateUserInput();
}//end of void call

}//end of main