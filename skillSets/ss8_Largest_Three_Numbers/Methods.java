import java.util.Scanner; 
public class Methods{
    //nonvalue returning method 
    public static void getRequirements(){
        //display operational messeges
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");
        System.out.println();
    }//end of get requirements

    public static void valildateUserInput(){
                //declare variables and create scanner object
        int num1 = 0, num2 = 0, num3 = 0;
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter first number: ");
        while(!sc.hasNextInt()){
            System.out.println("Not valid integer\n");
            sc.next(); //this is requried so there is not an infiniate loop
            System.out.print("Please try again. Enter first number: ");
        }//end of while

        num1 = sc.nextInt();

        System.out.print("\nPlease enter second number: ");
        while(!sc.hasNextInt()){            
            System.out.println("Not valid integer!\n");
            sc.next();//agian, needed for non infinate loop
            System.out.print("Please try again. Enter second number: ");
        }//and of while

        num2 = sc.nextInt();
        System.out.print("\nPlease enter third number: ");
        while(!sc.hasNextInt()){
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter third number: ");

        } //end of while

        num3 = sc.nextInt();
        System.out.println();

        getLargestNumber(num1, num2, num3);
    }//end of validateUserInput

    public static void getLargestNumber(int num1, int num2, int num3){
        System.out.println("Numbers entered: " + num1 + ", " + num2 + ", " + num3);
        if(num1 > num2 && num1 > num3) System.out.println(num1 + " is largest.");
        else if(num2 > num1 && num2 > num3) System.out.println(num2 + " is largest."); 
        else if(num3 > num1 && num3 > num2)System.out.println(num3 +" is the largest. ");
        else System.out.println("Integers are equal.");
    }
}//end of methods







/* 
import java.util.Scanner; 

public class Methods{
class LargestOfThreeNumbers{
    public static void main(String args[]){



    }//end of void

    public static void getLargestNumber(int num1, int num2, int num3)
    {

    }//end of getLargestNumber
}//end of largestOfNumbers class

*/