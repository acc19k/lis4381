//this program will ask the user for their name and age and then display it as a sentence. We are going to use a nested method in order to make that happen so we ask for name, age and display it.


import java.util.Scanner;

public class Methods {

	public static void getRequirements() {
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements.");
        System.out.println("1) getRequirements(): Void method displays program requirements.");
        System.out.println("2) getUserInput(): Void method prompts for user input, \n\tthen calls two methods: myVoidMethod()and myValueReturningMethod().");
        System.out.println("3) myVoidMethod():\n" + "\ta. Accepts two arguments:String and int. \n" + "\tb. prints user's first name and age.");
        System.out.println("4) myValueReturningMethod():\n" + "\ta. Accepts two arguments:String and int. \n" + "\tb. Returns String containing first name and age");
        System.out.println();

    }//end of getrequirements
		
		//we are going to begin the nested method
		public static void getUserInput() {

			//initialize a scanner to get user info
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Enter first name: ");
			String name = sc.next();

			
			System.out.print("\nEnter age: ");
			int age  = sc.nextInt();

			
			myVoidMethod(name, age);
			System.out.println(myValueReturningMethod(name, age));

		} //end of getUserInput

		
		//Now we have to create the method that is being called in the previous method. We also have to initialize the new local variables
		//since they are not being included in this method.
		
		public static void myVoidMethod(String name, int age) {

			System.out.println("\nvoid method call: " + name + " is " + age);
        }//end of myVoidMethod

		//now let's make the second method that is being called in the first method
		public static String myValueReturningMethod(String name, int age) {

			String methodReturn = "value returning method call: " + name + " is " + age;
			return methodReturn;
		} //end of myvalueReturningMethod


	
	


}//end of Methods