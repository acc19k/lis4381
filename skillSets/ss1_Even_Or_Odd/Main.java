//here we will call from the methods library in the other file 
//since the Methods file is in the same directory, we do not need to import anything (Methods file = package; used to group related classes and interfaces, and avoid name conflicts

public class Main {
    
    public static void main(String[] args)
    {
        //all we are doing in this folder is calling methods!
        Methods.getRequirements();
        Methods.evaluateNumber();

    }

} //end of main 