//this will be the methods library for this skill set aka the Main.java file will call to the  mathods here. 
//Problem: We have to create a program that asks user for an integer and returns whether the integer is even or odd

import java.util.Scanner; //this file we have to import from library

public class Methods
{

    //here is the first method that that inroduces people to the program. So, no object because it doesn't retun anything, just prints:
    public static void getRequirements()
    {
        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("This program evaluates integers as even or odd.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");

        System.out.println();
    } //end of getRequirements Method

    //This next method will ask for the integer and scan create a scanner to read it
    //it will also be a void method since we are not including a retur statement
    public static void evaluateNumber()
    {
        //create variable and change it to become whatever the user inputs from scanner
        int x = 0;
        System.out.println("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        x =sc.nextInt();  //basically calls for integer detected from scanner

        //now an if else statement to figure out if it is an even or odd number
        if (x % 2 == 0){
            System.out.println(x + " is an even number!");
        }
        else{
            System.out.println(x + " is an odd number!");
        }

    } //end of evaluateNumber method


} //end of methods