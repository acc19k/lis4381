import java.util.Scanner; 

public class Methods {

    public static void getRequirements(){

        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("Program requirements: ");
        System.out.println("1. Capture user-input.");
        System.out.println("2. Based upon the day of the week, determine if rain will occur. ;-) ");
        System.out.println("3. Program does case-sensitive comparison.");
        System.out.println();

    }//end of gerRequirements

    public static void getDayOfTheWeek(){

        Scanner sc = new Scanner(System.in);
        String myStr = " ";
        
        System.out.println("Input");
        System.out.println("Enter full day of the week: ");
        myStr = sc.nextLine();

        if(myStr.equalsIgnoreCase("monday") || myStr.equalsIgnoreCase("wednesday") || myStr.equalsIgnoreCase("friday")){
            System.out.println("\nOutput");
            System.out.println("Better get an umbrella!!");

        }//end of rainy if 
        else if(myStr.equalsIgnoreCase("tuesday") || myStr.equalsIgnoreCase("thursday")){
            System.out.println("\nOutput");
            System.out.println("Looks like it won't be raining today!");
        } 
        else if(myStr.equalsIgnoreCase("saturday") || myStr.equalsIgnoreCase("sunday")){
            System.out.println("\nOutput");
            System.out.println("It's the weekend! Go ahead and enjoy it! woop woop! :D");
        }
        else{
            System.out.println("Try again, please enter full name of day of the week.");
        }


    }//end of getDayOfTheWeek

}