<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This an online portfolio showing what I learned in LIS4381-Mobile Web Application Development.">
		<meta name="author" content="Andrea Colon-De Feria">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Healthy Recipes App</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> <br>This assignment required us to create a Healthy Recipes app on Android Studio and include screenshots of the two interfaces of the app. Alongside this, we completed three Java skillsets, of which there are screenshots available.
					<br><strong>Requirements: </strong>  
				</p>
				<ol>
					<li>Create a mobile recipe app using Android Studio</li>
					<li>Provide screenshots of completed app and Skill Sets 1-3</li>
					<li>Chapter Questions (Chs 3,4)</li>
				</ol>

				<h4>Screenshot 1 of Healthy Recipes app running on Pixel 4 API 30</h4>
				<img src="images/pixel4Snip1.png" class="img-responsive center-block" alt="App screenshot 1">

				<h4>Screenshot 2 of Healthy Recipes app running on Pixel 4 API 30</h4>
				<img src="images/pixel4Snip2.png" class="img-responsive center-block" alt="App screenshot 2">

				<h4>Java Skillset 1:Even or Odd</h4>
				<img src="/repos/lis4381/skillSets/ss1_Even_Or_Odd/ss1_img/ss1Screenshot.png" class="img-responsive center-block" alt="ss1">

				<h4>Java Skillset 2:Largest of Two Integers</h4>
				<img src="repos/lis4381/skillSets/ss2_Largest_Of_Two_Integers/ss2_img/ss2_Screenshot.png" class="img-responsive center-block" alt="ss2">

				<h4>Java Skillset 3:Arrays and Loops</h4>
				<img src="www/repos/lis4381/skillSets/ss3_Arrays_And_Loops/ss3_img/ss3_Screenshot.png" class="img-responsive center-block" alt="ss3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
