

# LIS4381 - Mobile Web Application Development

## Andrea Colon-De Feria

### Assignment 2 Requirements:

*Three parts:*

1. Create a mobile recipe app using Android Studio
2. Provide screenshots of completed app and Skill Sets 1-3
3. Chapter Questions (Chs 3,4)

### README.md file should include the following items:

* Screenshot of running applications' first interface
* Screenshot of running application's second interface
* Screenshot of skill sets 1-3

### Assignment Screenshots:

|*Screenshot 1 of Healthy Recipes app running on Pixel 4 API 30* | *Screenshot 2 of Healthy Recipes app running on Pixel 4 API 30* |
|-------------------------------------------------|-------------------------------------------------|
| [![Pixel 4 screenshot 1](images/pixel4Snip1.png)](images/pixel4Snip1.png) | [![Pixel 4 screenshot 2](images/pixel4Snip2.png)](images/pixel4Snip2.png) |
&nbsp;

&nbsp;


| *Screenshot 1 of Healthy Recipes app running on Nexus 5x API 24* |  *Screenshot 2 of Healthy Recipes app running on Nexus 5x API 24* |
|------------------------------------------------------------------|-------------------------------------------------------------------|
| [![Nexus 5 Screenshot 1](images/nexus5Snip1.png)](images/nexus5Snip1.png)   |[![Nexus 5 Screenshot 2](images/nexus5Snip2.png)](images/nexus5Snip2.png) |

&nbsp;

&nbsp;

### Java Skill Set Screenshots:
|  *Skill set 1: Even or odd*  |  *Skill set 2: Largest of two integers*  |  *Skill set 3: Arrays and loops*  |
|------------------------------|------------------------------------------|-----------------------------------|
| [![Skill set 1 Screenshot](/skillSets/ss1_Even_Or_Odd/ss1_img/ss1Screenshot.png)](/skillSets/ss1_Even_Or_Odd/ss1_img/ss1Screenshot.png)     |  [![Skill set 2 Screenshot](/skillSets/ss2_Largest_Of_Two_Integers/ss2_img/ss2_Screenshot.png)](/skillSets/ss2_Largest_Of_Two_Integers/ss2_img/ss2_Screenshot.png)     |[![Skill set 3 Screenshot](/skillSets/ss3_Arrays_And_Loops/ss3_img/ss3_Screenshot.png)](/skillSets/ss3_Arrays_And_Loops/ss3_img/ss3_Screenshot.png)|

Click a screenshot to view it larger.

